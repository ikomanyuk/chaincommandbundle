<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\Event;

/**
 * Class ChainEvents
 * List of all dispatched events
 * @package Ikomanyuk\ChainCommandBundle\Event
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ChainEvents
{
    /**
     * Event occurs immediatly after calling ChainManager::execute
     * @Event
     */
    const PREPARING = 'chain_manager.execute.preparing';

    /**
     * Event occurs before executing main chain
     * @Event
     */
    const RUNNING_ITSELF = 'chain_manager.execute.running_itself';

    /**
     * Event occurs before executing each sub chain
     * @Event
     */
    const EXECUTING_CHAIN = 'chain_manager.execute.chain';

    /**
     * Event occurs after everything done
     * @Event
     */
    const FINISHED = 'chain_manager.execute.finished';

    /**
     * Event occurs when sub chain fails
     * @Event
     */
    const FAILED_CHAIN = 'chain_manager.execute.failed_chain';
}
