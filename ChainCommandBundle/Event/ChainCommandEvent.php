<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\Event;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChainCommandEvent
 * Event class. Contains everethig what we are need =)
 * @package Ikomanyuk\ChainCommandBundle\Event
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 * @codeCoverageIgnore
 */
class ChainCommandEvent extends ConsoleCommandEvent
{
    /**
     * @var Command|null Command which executed right now.
     */
    private $currentCommand;

    public function __construct(Command $mainCommand, Command $currentCommand = null, InputInterface $input, OutputInterface $output)
    {
        parent::__construct($mainCommand, $input, $output);
        $this->currentCommand = $currentCommand;
    }

    /**
     * @return Command
     */
    public function getCurrentCommand()
    {
        return $this->currentCommand;
    }
}
