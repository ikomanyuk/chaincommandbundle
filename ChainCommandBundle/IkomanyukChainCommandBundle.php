<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle;

use Ikomanyuk\ChainCommandBundle\DependencyInjection\Compiler\ChainedCommandCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class IkomanyukChainCommandBundle
 *
 * @package Ikomanyuk\ChainCommandBundle
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class IkomanyukChainCommandBundle extends Bundle
{
    /**
     * @inheritdoc
     * @codeCoverageIgnore
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ChainedCommandCompilerPass());
    }
}
