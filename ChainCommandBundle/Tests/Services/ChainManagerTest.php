<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\Tests\Services;

use Ikomanyuk\ChainCommandBundle\Event\ChainCommandEvent;
use Ikomanyuk\ChainCommandBundle\Services\ChainManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ChainManagerTest
 *
 * @package Ikomanyuk\ChainCommandBundle\Tests\Services
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ChainManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ChainManager
     */
    protected $chainManager;

    /**
     * Preparing for test
     */
    protected function setUp()
    {
        /** @var EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $this->getMockBuilder('Symfony\\Component\\EventDispatcher\\EventDispatcherInterface')
            ->getMock();
        $this->chainManager = new ChainManager($eventDispatcher);
    }

    /**
     * Provider give to us correct data for chains
     * @return array correct data format
     */
    public function correctChainsProvider()
    {
        return [
            [
                [
                    'app:foo' => [
                        'App\\Command\\Test1',
                        'App\\Command\\Test2',
                    ]
                ]
            ]
        ];
    }

    /**
     * Trash data
     * @return array everything but not correct
     */
    public function incorrectChainsProvider()
    {
        return [
            [
                [], 'Chains cannot be empty.'
            ],
            [
                [
                    'empty value' => []
                ],  'Chain and values cannot be blank'
            ],
            [
                [
                    'value is not array' => 'but string'
                ],  'Looks like you have called method with one dimensional array.'
            ],
            [
                [
                    12 => 'key not string'
                ],  'Looks like you have called method with one dimensional array.'
            ]
        ];
    }

    /**
     * Testing work with correct data
     * @dataProvider correctChainsProvider
     */
    public function testSetCorrectChains($chain)
    {
        $this->chainManager->setChains($chain);
        $this->assertTrue($this->chainManager->isInitialized());
    }

    /**
     * Testing work with trash data
     * @dataProvider correctChainsProvider
     */
    public function testSetChainsFailsWithLogicException($chain)
    {
        $this->expectException(\LogicException::class);
        $this->chainManager->setChains($chain);
        $this->chainManager->setChains($chain);
    }

    /**
     * Testing work with trash data
     * @dataProvider incorrectChainsProvider
     */
    public function testSetChainsFailsWithRuntimeException($chain, $exceptionMessage = '')
    {
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage($exceptionMessage);
        $this->chainManager->setChains($chain);
    }

    /**
     * Test for ChainManager::isChainedCommandClass
     * @param array $chain
     * @dataProvider correctChainsProvider
     */
    public function testIsChainedCommandClass($chain)
    {
        $this->chainManager->setChains($chain);

        $this->assertTrue($this->chainManager->isChainedCommandClass('App\\Command\\Test1'));
        $this->assertFalse($this->chainManager->isChainedCommandClass('App\\Command\\Test3'));

        $this->expectException(\InvalidArgumentException::class);
        $this->chainManager->isChainedCommandClass(new \StdClass());
        $this->chainManager->isChainedCommandClass(['array']);
        $this->chainManager->isChainedCommandClass(12);
    }

    /**
     * Test for ChainManager::isMainChain
     * @param array $chain
     * @dataProvider correctChainsProvider
     */
    public function testIsMainChain($chain)
    {
        $this->chainManager->setChains($chain);

        $this->assertTrue($this->chainManager->isMainChain('app:foo'));
        $this->assertFalse($this->chainManager->isMainChain('app:foo2'));

        $this->expectException(\InvalidArgumentException::class);
        $this->chainManager->isMainChain(new \StdClass());
        $this->chainManager->isMainChain(['array']);
        $this->chainManager->isMainChain(13);
    }

    /**
     * Test for ChainManager::testGetMainChains
     * @param $chain
     * @dataProvider correctChainsProvider
     */
    public function testGetMainChains($chain)
    {
        $this->chainManager->setChains($chain);

        $this->assertSame([], $this->chainManager->getMainChains('App\\Command\\Test3'));
        $this->assertSame(['app:foo'], $this->chainManager->getMainChains('App\\Command\\Test1'));

        $this->expectException(\InvalidArgumentException::class);
        $this->chainManager->getMainChains(new \StdClass());
        $this->chainManager->getMainChains(['array']);
        $this->chainManager->getMainChains(12);
    }

    /**
     * Test for ChainManager::getChains
     * @param $chain
     * @dataProvider correctChainsProvider
     */
    public function testGetChains($chain)
    {
        $this->chainManager->setChains($chain);

        $this->assertSame($chain['app:foo'], $this->chainManager->getChains('app:foo'));

        $this->expectException(\InvalidArgumentException::class);
        $this->chainManager->getChains('');
        $this->chainManager->getChains(new \DateTime());
        $this->chainManager->getChains('daa');
    }

    /**
     * Testing protected but most important method
     */
    public function testTryExecuteChain()
    {
        $eventDispatcher = $this->getMockBuilder('Symfony\\Component\\EventDispatcher\\EventDispatcherInterface')
            ->getMock();

        $eventDispatcher->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->equalTo('chain_manager.execute.chain'),
                $this->isInstanceOf(ChainCommandEvent::class)
            );

        /** @var EventDispatcherInterface $eventDispatcher */
        $this->chainManager = new ChainManager($eventDispatcher);

        $mainCommand = $this->getMockBuilder('Symfony\\Component\\Console\\Command\\Command')
            ->disableOriginalConstructor()
            ->getMock();

        $subCommand = $this->getMockBuilder('Symfony\\Component\\Console\\Command\\Command')
            ->disableOriginalConstructor()
            ->getMock();

        $output = $this->getMockBuilder('Symfony\\Component\\Console\\Output\\OutputInterface')
            ->getMock();

        $subCommand->expects($this->any())
            ->method('getName')
            ->willReturn('app:foo');

        $subCommand->expects($this->once())
            ->method('run')
            ->with(
                $this->isInstanceOf(ArrayInput::class),
                $this->isInstanceOf(OutputInterface::class)
            )->willReturn(0);

        $result = $this->invokeMethod($this->chainManager, 'tryExecuteChain', [
            $mainCommand,
            $subCommand,
            $output
        ]);

        $this->assertSame(0, $result);
    }

    /**
     * Testing protected but most important method for correct work with exceptions
     */
    public function testTryExecuteChainException()
    {
        $eventDispatcher = $this->getMockBuilder('Symfony\\Component\\EventDispatcher\\EventDispatcherInterface')
            ->getMock();

        $eventDispatcher
            ->method('dispatch')
            ->with(
                $this->equalTo('chain_manager.execute.failed_chain'),
                $this->isInstanceOf(ChainCommandEvent::class)
            );

        /** @var EventDispatcherInterface $eventDispatcher */
        $this->chainManager = new ChainManager($eventDispatcher);

        $mainCommand = $this->getMockBuilder('Symfony\\Component\\Console\\Command\\Command')
            ->disableOriginalConstructor()
            ->getMock();

        $subCommand = $this->getMockBuilder('Symfony\\Component\\Console\\Command\\Command')
            ->disableOriginalConstructor()
            ->getMock();

        $output = $this->getMockBuilder('Symfony\\Component\\Console\\Output\\OutputInterface')
            ->getMock();

        $subCommand->expects($this->any())
            ->method('getName')
            ->willReturn('app:foo');

        $subCommand
            ->method('run')
            ->will($this->throwException(new \Exception('Test exception', 13)));

        $this->invokeMethod($this->chainManager, 'tryExecuteChain', [
            $mainCommand,
            $subCommand,
            $output
        ]);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object    Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array  $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
