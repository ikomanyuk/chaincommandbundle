<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\Tests\EventListener;

use Ikomanyuk\ChainCommandBundle\EventListener\ExecuteCommandListener;
use Ikomanyuk\ChainCommandBundle\Services\ChainManager;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Testing correct work for chained commands
 *
 * Class ExecuteCommandListenerTest
 *
 * @package Ikomanyuk\ChainCommandBundle\Tests\EventListener
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ExecuteCommandListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * We are should work with initialized manager.
     */
    public function testShouldMarkManagerAsInitialized()
    {
        $chainManager = $this->getMockBuilder(ChainManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $chainManager->method('isInitialized')->willReturn(false);
        $chainManager->method('isChainedCommandClass')->willReturn(false);
        $chainManager->method('isMainChain')->willReturn(false);

        $command = $this->getMockBuilder(Command::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command->method('getName')->willReturn('app:fooo');
        $input = $this->getMockBuilder(InputInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $output = $this->getMockBuilder(OutputInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event = new ConsoleCommandEvent($command, $input, $output);
        $listener = new ExecuteCommandListener($chainManager);
        $listener->onConsoleCommand($event);
    }

    /**
     * Testing for correct work when current command is main chain
     */
    public function testMainChain()
    {
        $chainManager = $this->getMockBuilder(ChainManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $chainManager->method('isInitialized')->willReturn(true);
        $chainManager->method('isChainedCommandClass')->willReturn(false);
        $chainManager->method('isMainChain')->willReturn(true);

        $command = $this->getMockBuilder(Command::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command->method('getName')->willReturn('app:fooo');

        $input = $this->getMockBuilder(InputInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $output = $this->getMockBuilder(OutputInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $event = $this->getMockBuilder(ConsoleCommandEvent::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event->expects($this->once())
            ->method('disableCommand');
        $event->method('getCommand')->willReturn($command);
        $event->method('getInput')->willReturn($input);
        $event->method('getOutput')->willReturn($output);

        $listener = new ExecuteCommandListener($chainManager);
        $listener->onConsoleCommand($event);
    }

    /**
     * testing for correct work when currenct command is sub chain
     */
    public function testChainedCommand()
    {
        $chainManager = $this->getMockBuilder(ChainManager::class)
            ->disableOriginalConstructor()
            ->getMock();
        $chainManager->method('isInitialized')->willReturn(true);
        $chainManager->method('isChainedCommandClass')->willReturn(true);
        $chainManager->method('isMainChain')->willReturn(false);
        $chainManager->method('getMainChains')->willReturn(['app:main']);

        $command = $this->getMockBuilder(Command::class)
            ->disableOriginalConstructor()
            ->getMock();
        $command->method('getName')->willReturn('app:fooo');

        $output = $this->getMockBuilder(OutputInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $event = $this->getMockBuilder(ConsoleCommandEvent::class)
            ->disableOriginalConstructor()
            ->getMock();
        $event->method('getCommand')->willReturn($command);
        $event->expects($this->once())
            ->method('disableCommand');
        $event->expects($this->once())
            ->method('stopPropagation');
        $event->method('getOutput')->willReturn($output);



        $listener = new ExecuteCommandListener($chainManager);
        $listener->onConsoleCommand($event);
    }
}
