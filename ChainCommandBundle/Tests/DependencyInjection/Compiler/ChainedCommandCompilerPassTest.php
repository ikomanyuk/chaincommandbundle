<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */


namespace Ikomanyuk\ChainCommandBundle\Tests\DependencyInjection\Compiler;

use Ikomanyuk\ChainCommandBundle\DependencyInjection\Compiler\ChainedCommandCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class ChainedCommandCompilerPassTest
 *
 * @package Ikomanyuk\ChainCommandBundle\Tests\DependencyInjection\Compiler
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ChainedCommandCompilerPassTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing process method
     */
    public function testProcess()
    {
        $container = $this->getMockBuilder('Symfony\Component\DependencyInjection\ContainerBuilder')
            ->setMethods(['findTaggedServiceIds'])
            ->getMock()
        ;
        $container->expects($this->atLeast(1))
            ->method('findTaggedServiceIds')
            ->willReturn([
                'app.command.foo' => [0 => ['main_chain' => 'app:mainchain']]
            ])
        ;

        $definitnion = $this->getMockBuilder('Symfony\Component\DependencyInjection\Definition')
            ->disableOriginalConstructor()
            ->getMock();
        $definitnion->expects($this->atLeastOnce())
            ->method('getClass')
            ->willReturn('App\Command\FooCommand')
        ;

        /** @var ContainerBuilder $container */
        $container->setDefinition('app.command.foo', $definitnion);

        $definitnion = $this->getMockBuilder('Symfony\Component\DependencyInjection\Definition')
            ->disableOriginalConstructor()
            ->getMock();
        $definitnion->expects($this->atLeastOnce())
            ->method('addMethodCall')
        ;

        $container->setDefinition('ikomanyuk.chained.manager', $definitnion);


        $pass = new ChainedCommandCompilerPass();
        $pass->process($container);
    }

    /**
     * Testing expected exceptions
     */
    public function testProcessWillThrowException()
    {
        $container = new ContainerBuilder();
        $container->addCompilerPass(new ChainedCommandCompilerPass());

        $this->expectException(\LogicException::class);
        $this->expectExceptionMessage('You asked me to create chained command, but you do not tell me what to use as main_chain.');

        $def = new Definition('App\Command\FooCommand');
        $def->addTag('chained.command');
        $container->addDefinitions([$def]);

        $container->compile();
    }
}
