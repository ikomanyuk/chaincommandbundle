<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ChainedCommandCompilerPass
 * This compiler collects all services that tagged as chained.command
 * @package Ikomanyuk\ChainCommandBundle\DependencyInjection\Compiler
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ChainedCommandCompilerPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        $chainedCommands = $container->findTaggedServiceIds('chained.command');
        $chains = [];
        foreach ($chainedCommands as $id => $tags) {
            $definition = $container->getDefinition($id);

            foreach ($tags as $t) {
                if (empty($t['main_chain'])) {
                    throw new \LogicException(
                        "You asked me to create chained command, but you do not tell me what to use as main_chain."
                    );
                }

                $chains[$t['main_chain']][] = $definition->getClass();
            }
        }

        $definition = $container->getDefinition('ikomanyuk.chained.manager');
        $definition->addMethodCall('setChains', [$chains]);
    }
}
