<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\Services;

use Ikomanyuk\ChainCommandBundle\Event\ChainCommandEvent;
use Ikomanyuk\ChainCommandBundle\Event\ChainEvents;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class ChainManager
 *
 * @package Ikomanyuk\ChainCommandBundle\Services
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ChainManager
{
    /**
     * @var array Contains all registered chains.
     */
    private $chains = [];

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var bool Flag which should be set after setChains method call.
     */
    private $isInitialized = false;

    /**
     * @var int Last exit code of command
     */
    private $chainExitCode = 0;

    /**
     * ChainManager constructor.
     * @param EventDispatcherInterface $eventDispatcherInterface
     */
    public function __construct(EventDispatcherInterface $eventDispatcherInterface)
    {
        $this->eventDispatcher = $eventDispatcherInterface;
    }

    /**
     * Returns last exit code.
     *
     * @codeCoverageIgnore
     * @return int
     */
    public function getChainExitCode()
    {
        return $this->chainExitCode;
    }

    /**
     * Is ChainManager initialized or not.
     *
     * @return bool
     */
    public function isInitialized()
    {
        return $this->isInitialized;
    }

    /**
     * After call this you will not be able to set any new chain
     */
    public function markAsInitialized()
    {
        $this->isInitialized = true;
    }

    /**
     * @param array $chains Multidimensional array. Contains main chain as key and array of classes as value:
     *          $chains = [
     *              'app:foo' => [
     *                  'BarCommand',
     *                  'BazCommand',
     *              ]
     *          ]
     */
    public function setChains(array $chains)
    {
        if ($this->isInitialized()) {
            throw new \LogicException("Looks like you have called me twice. Fix this.");
        }

        if (empty($chains)) {
            throw new \RuntimeException("Chains cannot be empty.");
        }

        foreach ($chains as $chain => $values) {
            if (empty($chain) || empty($values)) {
                throw new \RuntimeException("Chain and values cannot be blank");
            }
            if (!is_string($chain) || !is_array($values)) {
                throw new \RuntimeException("Looks like you have called method with one dimensional array.");
            }
        }

        $this->chains = $chains;

        $this->markAsInitialized();
    }

    /**
     * Checks if param present in chains as chain.
     *
     * @param $commandClass
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function isChainedCommandClass($commandClass)
    {
        if (!is_string($commandClass) || empty($commandClass)) {
            throw new \InvalidArgumentException('Invalid argument');
        }

        foreach ($this->chains as $chain => $values) {
            if (in_array($commandClass, $values)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if command is present in chain as main chain.
     *
     * @param $commandName
     * @return bool
     * @throws \InvalidArgumentException
     */
    public function isMainChain($commandName)
    {
        if (!is_string($commandName) || empty($commandName)) {
            throw new \InvalidArgumentException('Invalid argument');
        }

        return in_array($commandName, array_keys($this->chains));
    }

    /**
     * Return list of commands in which class present as sub chain
     *
     * @param $commandClass
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getMainChains($commandClass)
    {
        if (!is_string($commandClass) || empty($commandClass)) {
            throw new \InvalidArgumentException('Invalid argument');
        }

        $chains = [];

        foreach ($this->chains as $id => $val) {
            if (in_array($commandClass, $val)) {
                $chains[] = $id;
            }
        }

        return $chains;
    }

    /**
     * Returns array of sub chains
     *
     * @param $commandName
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getChains($commandName)
    {
        if (!is_string($commandName) || empty($commandName) || empty($this->chains[$commandName])) {
            throw new \InvalidArgumentException('Invalid argument');
        }

        return $this->chains[$commandName];
    }

    /**
     * Trying to run main chain and all sub chains
     *
     * @param Command $command Main chain
     * @param InputInterface $input console input
     * @param OutputInterface $output
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    public function execute(Command $command, InputInterface $input, OutputInterface $output)
    {
        $event = new ChainCommandEvent($command, null, $input, $output);
        $this->eventDispatcher->dispatch(ChainEvents::PREPARING, $event);

        $application = $command->getApplication();
        $allCommands = array_flip(array_map(function ($obj) {return get_class($obj);}, $application->all()));
        $chainedCommands = [];
        foreach ($this->getChains($command->getName()) as $chain) {
            if (!empty($allCommands[$chain])) {
                $chainedCommands[] = $allCommands[$chain];
            } else {
                throw new \RuntimeException(sprintf(
                    "Command %s registered but not present", $chain
                ));
            }
        }

        $event = new ChainCommandEvent($command, null, $input, $output);
        $this->eventDispatcher->dispatch(ChainEvents::RUNNING_ITSELF, $event);
        $this->chainExitCode = $command->run($input, $output);

        foreach ($chainedCommands as $com) {
            $this->chainExitCode = $this->tryExecuteChain($command, $application->get($com), $output);
        }

        $event = new ChainCommandEvent($command, null, $input, $output);
        $this->eventDispatcher->dispatch(ChainEvents::FINISHED, $event);
    }

    /**
     * Trying to run all sub chains
     *
     * @param Command $command
     * @param Command $com
     * @param OutputInterface $output
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    private function tryExecuteChain(Command $command, Command $com, OutputInterface $output)
    {
        try {
            $input = new ArrayInput(['command' => $com->getName()]);

            $event = new ChainCommandEvent($command, $com, $input, $output);
            $this->eventDispatcher->dispatch(ChainEvents::EXECUTING_CHAIN, $event);

            $exitCode = $com->run($input, $output);
        } catch (\Exception $e) {
            $event = new ChainCommandEvent($command, $com, $input, $output);
            $this->eventDispatcher->dispatch(ChainEvents::FAILED_CHAIN, $event);

            $exitCode = $e->getCode();
        }

        return $exitCode;
    }
}
