<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\EventListener;

use Ikomanyuk\ChainCommandBundle\Services\ChainManager;
use Symfony\Component\Console\Event\ConsoleCommandEvent;

/**
 * Class ExecuteCommandListener
 * Event listener on each command run
 *
 * @package Ikomanyuk\ChainCommandBundle\EventListener
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 */
class ExecuteCommandListener
{

    /**
     * @var ChainManager
     */
    private $manager;

    /**
     * ExecuteCommandListener constructor.
     * @param ChainManager $manager
     * @codeCoverageIgnore
     */
    public function __construct(ChainManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Listening for chained commands
     *
     * @param ConsoleCommandEvent $event
     */
    public function onConsoleCommand(ConsoleCommandEvent $event)
    {
        if ($event->isPropagationStopped()) {
            return;
        }

        $commandName = $event->getCommand()->getName();
        $commandClass = get_class($event->getCommand());

        if (!$this->manager->isInitialized()) {
            $this->manager->markAsInitialized();
        }

        if ($this->manager->isChainedCommandClass($commandClass)) {
            $event->disableCommand();
            $event->stopPropagation();

            $mainChains = $this->manager->getMainChains($commandClass);

            $event->getOutput()->writeln(sprintf(
                "Command %s marked as chain for this commands [%s] and cannot be more executed separately",
                $commandName,
                implode(', ', $mainChains)
            ));
        }

        if ($this->manager->isMainChain($commandName)) {
            $event->disableCommand();

            $this->manager->execute($event->getCommand(), $event->getInput(), $event->getOutput());
        }
    }
}
