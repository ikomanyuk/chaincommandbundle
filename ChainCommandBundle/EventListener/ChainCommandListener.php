<?php

/**
 * Demo project
 *
 * @license https://tldrlegal.com/license/mit-license
 */

namespace Ikomanyuk\ChainCommandBundle\EventListener;

use Ikomanyuk\ChainCommandBundle\Event\ChainCommandEvent;
use Ikomanyuk\ChainCommandBundle\Services\ChainManager;
use Psr\Log\LoggerInterface;

/**
 * Class ChainCommandListener
 * Event listener on internal events
 * @package Ikomanyuk\ChainCommandBundle\EventListener
 * @author Igor Komanyuk <ikomanyuk@gmail.com>
 * @codeCoverageIgnore
 */
class ChainCommandListener
{

    /**
     * @var ChainManager
     */
    private $manager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ChainCommandListener constructor.
     *
     * @param ChainManager $manager
     * @param LoggerInterface $logger
     */
    public function __construct(ChainManager $manager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    /**
     * Listen event chain_manager.execute.preparing
     *
     * @param ChainCommandEvent $event
     */
    public function onPreparing(ChainCommandEvent $event)
    {
        $command = $event->getCommand()->getName();
        $this->logger->log(200, sprintf('Preparing to run %s', $command));
    }

    /**
     * Listen event chain_manager.execute.running_itself
     *
     * @param ChainCommandEvent $event
     */
    public function onRunningItself(ChainCommandEvent $event)
    {
        $command = $event->getCommand()->getName();
        $this->logger->log(200, sprintf('Running %s', $command));
    }

    /**
     * Listen event chain_manager.execute.chain
     *
     * @param ChainCommandEvent $event
     */
    public function onChain(ChainCommandEvent $event)
    {
        $this->logger->log(200, sprintf(
            "%s registered as a member of %s",
            $event->getCurrentCommand()->getName(),
            $event->getCommand()->getName()
        ));
    }

    /**
     * Listen event chain_manager.execute.finished
     *
     * @param ChainCommandEvent $event
     */
    public function onFinished(ChainCommandEvent $event)
    {
        $this->logger->log(200, sprintf("%s finished work", $event->getCommand()->getName()));
    }
}
